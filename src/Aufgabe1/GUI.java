package Aufgabe1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    private JPanel mainPanel;
    private JButton RUNButton;
    private JButton RUNButton1;
    private JButton RUNButton2;
    private JButton RUNButton3;
    private JTextField txtNum1;
    private JTextField txtNum2;
    private JTextField txtSumOut;
    private JTextField txtDifferenceOut;
    private JTextField txtMultiplyOut;
    private JTextField txtDivide;


    public GUI(String title) {
        super((title));

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.pack();


        RUNButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                double a = Double.parseDouble(txtNum1.getText());
                double b = Double.parseDouble(txtNum2.getText());

                txtSumOut.setText(methods.add(a, b) + "");


            }
        });
        RUNButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                double a = Double.parseDouble(txtNum1.getText());
                double b = Double.parseDouble(txtNum2.getText());

                txtDifferenceOut.setText(methods.subtract(a, b) + "");
            }
        });
        RUNButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                double a = Double.parseDouble(txtNum1.getText());
                double b = Double.parseDouble(txtNum2.getText());

                txtMultiplyOut.setText(methods.multiply(a, b) + "");
            }
        });
        RUNButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                double a = Double.parseDouble(txtNum1.getText());
                double b = Double.parseDouble(txtNum2.getText());

                txtDivide.setText(methods.divide(a, b) + "");
            }
        });
    }

    public static void main(String[] args) {

        JFrame frame = new GUI("GUI");
        frame.setVisible(true);
    }
}
